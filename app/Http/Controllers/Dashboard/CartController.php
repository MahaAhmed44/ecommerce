<?php

namespace App\Http\Controllers\Dashboard;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CartController extends Controller
{
    public function index(Request $request)
    {

    	/*$orders = Order::whereHas('client',function($q) use ($request){
           return $q->where('name','like','%'.$request->search.'%');
        })->get();

        return view('dashboard.carts.index',compact('orders'));*/
        
    	$orders = Order::paginate(5);
    	return view('dashboard.carts.index',compact('orders'));
    }

    public function products(Order $order)
    {
        $products = $order->products;
        return view('dashboard.carts._products',compact('products'));
    }


    public function destroy(Order $order)
    {

    }
}
