<?php

namespace App\Http\Controllers\Dashboard;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Order;
use App\Product;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        
    }

    public function create(Client $client)
    {
    	 $categories = Category::with('products')->get();
    	 return view('dashboard.orders.create',compact('client','categories'));
    }

    public function store(Request $request,Client $client)
    {

    	$request->validate([

             'products'    =>  'required|array',
            // 'quantities'    =>  'required|array',
            ]);

        $order = $client->orders()->create([]);

        $order->products()->attach($request->products);

         $total_price = 0;

        foreach ($request->products as $id=>$quantity)
        {
            //dd($quantity['quantity']);
            $product = Product:: FindOrFail($id);
            $total_price += $product->sale_price * $quantity['quantity'];

            //$order->products()->attach($product_id,['quantity' => $request->quantities[$id]]);
           
           $product->update([
                'stock'  => $product->stock - $quantity['quantity']
            ]);

        } // end of foreach

         $order->update([
            'total_price' => $total_price
            ]);

         
        session()->flash('success',__('site.added.successfully'));
        return redirect()->route('dashboard.carts.index');
    }

    public function edit(Client $client,Order $order)
    {
    	
    }

    public function update(Request $request,Client $client,Order $order)
    {
    	
    }

    public function destroy(Client $client,Order $order)
    {
    	
    }
}
