<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use App\User;
use App\Client;
class DashboardController extends Controller
{
    public function index()
    {
    	$categories_count = Category::count();
    	$products_count = Product::count();
    	$clients_count = Client::count();
    	$users_count = User::whereRoleIs('admin')->count();
    	return view('dashboard.index',compact('categories_count','products_count','clients_count','users_count'));
    }
}