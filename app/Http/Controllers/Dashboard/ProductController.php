<?php

namespace App\Http\Controllers\Dashboard;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image as Image;
use App\Category;
use Storage;


class ProductController extends Controller
{
    
    public function index(Request $request)
    {
        $categories = Category::all();
        $products = Product::when($request->search,function($q) use ($request) {
           return $q->where('name','like','%'.$request->search.'%');
        })->when($request->category_id,function($q) use ($request) {
            return $q->where('category_id',$request->category_id);
        })->latest()->paginate(5);
        return view('dashboard.products.index',compact('categories','products'));
    }

    
    public function create()
    {
        $categories = Category::all();
         return view('dashboard.products.create',compact('categories'));
    }

    
    public function store(Request $request)
    {
       
        $rules = [
          'category_id'  => 'required',
          'name'        =>  'required',
          'description'        =>  'required',
          'purchase_price' => 'required',
          'sale_price' => 'required',
          'stock' => 'required',
        ];

        $request->validate($rules);

        $request_data = $request->all();
        if($request->image)
        {
          Image::make($request->image)->resize(300, null, function ($constraint) {
          $constraint->aspectRatio();
          })->save(public_path('uploads/product_images/'.$request->image->hashName()));
          $request_data['image'] = $request->image->hashName();
        } // end of if
        

        Product::create($request_data);
        session()->flash('success',__('site.added_successfully'));
        return redirect()->route('dashboard.products.index');
    }

    
    public function show(Product $product)
    {
        //
    }

    
    public function edit(Product $product)
    {
        $categories = Category::all();
        return view('dashboard.products.edit',compact('categories','product'));
    }

    
    public function update(Request $request, Product $product)
    {
         $rules = [
          'category_id'  => 'required',
          'name'        =>  'required|unique:categories,name',
          'description'        =>  'required',
          'purchase_price' => 'required',
          'sale_price' => 'required',
          'stock' => 'required',
        ];

        $request->validate($rules);

        $request_data = $request->all();

        /*if ($product->image != 'default.jpg')
        {
          Storage::disk('public_uploads')->delete('/product_images/'.$product->image);
        }*/ // end of if

        if($request->image)
        {
          Image::make($request->image)->resize(300, null, function ($constraint) {
          $constraint->aspectRatio();
          })->save(public_path('uploads/product_images/'.$request->image->hashName()));
          $request_data['image'] = $request->image->hashName();
        } // end of if

        $product->update( $request_data);
        session()->flash('success',__('site.updated_successfully'));
        return redirect()->route('dashboard.products.index');
    }

    
    public function destroy(Product $product)
    {
        if ($product->image != 'default.jpg')
        {
          Storage::disk('public_uploads')->delete('/product_images/'.$product->image);
        } // end of if

        $product->delete();
        session()->flash('success',__('site.deleted.successfully'));
        return redirect()->route('dashboard.products.index');
    }
}
