$(document).ready(function() {
     
     // add product btn
   $('.add-product-btn').on('click',function(e) {
      
      e.preventDefault();
      
      var name = $(this).data('name');
      var id = $(this).data('id');
      //var price = $(this).data('price');
      var price = $.number($(this).data('price'),2);

      $(this).removeClass('btn-info').addClass('btn-default disabled');
      
      //<input type="hidden" name="products_ids[]" value="${id}">
      var html = `<tr>
            <td>${name}</td>
            
            <td><input type="number" name="products[${id}][quantity]" data-price="${price}" class="form-control input-sm product-quantity" min="1" value="1"></td>
            <td class="product-price">${price}</td>
            <td><button class="btn btn-danger btn-sm reomve-product-btn" data-id="${id}"><span class="fa fa-trash"></span></button></td>
      </tr>`
     
      $('.order-list').append(html);
       calculateTotal();
   });

   // disabled btn
   $('body').on('click','.disabled',function(e) {
      
       e.preventDefault();
      
       }); // end of disabled

   // remove product btn
   $('body').on('click','.reomve-product-btn',function(e) {
      
       e.preventDefault();
       var id = $(this).data('id');
       $(this).closest('tr').remove();
       $('#product-'+id).removeClass('btn-default disabled').addClass('btn-info');
       
      
    calculateTotal();
   }); // end of product btn

   // change product quantity
   $('body').on('keyup change','.product-quantity',function() {

      var quantity = parseInt($(this).val());
      //var unitPrice = $(this).data('price');
      var unitPrice = parseFloat($(this).data('price').replace(/,/g, ''));  
      $(this).closest('tr').find('.product-price').html($.number(quantity * unitPrice,2));
      calculateTotal();

   }); // end of product quantity change


 }); // end of document ready


  // calculate the total
function calculateTotal()
{
	var price = 0;
	$('.order-list .product-price').each(function(index) {
       
     price += parseFloat($(this).html().replace(/,/g, ''));  
    
	});

	$('.total-price').html($.number(price,2));

	//check if price > 2
	if (price > 0)
	{
       $('#add-order-form-btn').removeClass('disabled');
	}
	else
	{
      $('#add-order-form-btn').addClass('disabled');
	}
} // end of calculate total