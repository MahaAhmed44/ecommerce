<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clients = ['rana','ragda'];

        foreach ($clients as $client)
        {
        	\App\Client::create([
                 'name'   => $client,
                 'phone'  => '011111111',
                 'address' => 'haram',
        	]);
        }
    }
}
