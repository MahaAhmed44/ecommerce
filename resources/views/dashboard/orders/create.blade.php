@extends('layouts.dashboard.app')

@section('content')
     
     <div class="content-wrapper">
      <section class="content-header">
   	   <h1>@lang('site.clients')</h1>
   	   <ol class="breadcrumb" style="float: right;">
          <li><a href="{{ route('dashboard.index') }}"><i class="nav-icon fas fa-tachometer-alt"></i>@lang('site.dashboard')</a></li>
          <li><a href="{{ route('dashboard.clients.index') }}">@lang('site.clients')</a></li>
         
          <li class="active">@lang('site.add')</li>
        </ol>
   	   </section>
         <br>
   	   <section class="content">
      <div class="row">
         <div class="col-md-6">
   	   	    <div class="card card-primary">
               <div class="card-header bg-gradient-danger">
                  <h3 class="card-title">@lang('site.categories')</h3>
               </div> <!-- end of box header -->
               <div class="card-body">
                 @foreach ($categories as $category)
                   <div class="panel-group">
                      <div class="panel panel-info">
                        <div class="panel-heading">
                           <h4 class="panel-title">
                              <a data-toggle="collapse" href="#{{str_replace(' ','-',$category->name)}}">{{$category->name}}</a>
                           </h4>
                        </div>
                        <div id="{{str_replace(' ','-',$category->name)}}" class="panel-collapse collapse">
                          <div class="panel-body">
                            @if ($category->products->count() > 0)
                              <table class="table table-hover">
                                <tr>
                                  <th>@lang('site.name')</th>
                                  <th>@lang('site.stock')</th>
                                  <th>@lang('site.price')</th>
                                  <th>@lang('site.add')</th>
                                </tr>
                                @foreach ($category->products as $product)
                                  <tr>
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->stock}}</td>
                                    <td>{{ number_format($product->sale_price)}}</td>
                                    <td><a href="#" class="btn btn-info btn-sm add-product-btn"
                                    id="product-{{$product->id}}" 
                                    data-name="{{$product->name}}"
                                    data-id="{{$product->id}}"
                                    data-price="{{$product->sale_price}}"
                                    ><i class="fa fa-plus"></i></a></td>
                                  </tr>
                                @endforeach
                              </table>
                              @else
                               <h2>@lang('site.no_records')</h2>
                              @endif
                          </div>
                        </div>
                      </div>
                   </div>
                 @endforeach
                  
               </div> <!-- end of card body -->
               
            </div>  <!-- end of card -->
        </div> <!-- end of col -->

        <div class="col-md-6">
            <div class="card card-primary">
               <div class="card-header bg-gradient-danger">
                  <h3 class="card-title">@lang('site.orders')</h3>
               </div> <!-- end of box header -->
               <div class="card-body">
                <form action="{{ route('dashboard.orders.store',$client->id) }}" method="post">

                 {{ csrf_field() }}
                {{ method_field('post') }}

                   <table class="table table-hover">
                  <thead>
                      <tr>
                        <th>@lang('site.product')</th>
                        <th>@lang('site.quantity')</th>
                        <th>@lang('site.price')</th>
                      </tr> 
                      </thead>
                      <tbody class="order-list">
                        
                      </tbody>
                  </table>
                 
                  <h4>@lang('site.total') : <span class="total-price"></span></h4>
                  <button class="btn btn-info btn-block disabled" id="add-order-form-btn"><i class="fa fa-plus"></i>@lang('site.add_order')</button>
                </form>
                 
                 
               </div> <!-- end of card body -->
               </div>  <!-- end of card -->
           
        </div> <!-- end of col -->

        </div>
   	   </section>
   </div>
@endsection