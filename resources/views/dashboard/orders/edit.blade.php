@extends('layouts.dashboard.app')

@section('content')
     
     <div class="content-wrapper">
      <section class="content-header">
       <h1>@lang('site.clients')</h1>
       <ol class="breadcrumb" style="float: right;">
          <li><a href="{{ route('dashboard.index') }}"><i class="nav-icon fas fa-tachometer-alt"></i>@lang('site.dashboard')</a></li>&nbsp
          <li><a href="{{ route('dashboard.clients.index') }}">@lang('site.clients')</a></li>&nbsp
          <li class="active">@lang('site.edit')</li>
        </ol>
       </section>
         <br>
       <section class="content">
            <div class="card card-primary">
               <div class="card-header bg-gradient-danger">
                  <h3 class="card-title">@lang('site.edit')</h3>
               </div> <!-- end of box header -->
               <div class="card-body">
               @include('partials._errors')
                  <form action="{{ route('dashboard.clients.update',$client->id)}}" method="post">
                     {{ csrf_field() }}
                     {{ method_field('put') }}
                     <div class="form-group">
                       <label>@lang('site.name')</label>
                       <input type="text" name="name" class="form-control" value="{{ $client->name }}" >
                     </div>

                     @for ($i = 0; $i < 2; $i++)
                      <div class="form-group">
                          <label>@lang('site.phone')</label>
                         <input type="text" name="phone[]" class="form-control" value="{{ $client->phone[$i] ?? ''}}">
                     </div>
                     @endfor

                      <div class="form-group">
                       <label>@lang('site.address')</label>
                       <textarea name="address" class="form-control">{{ $client->address }}</textarea>
                     </div>
                      

                     <div class="form-group">
                       <button type="submit" class="btn bg-gradient-danger"><i class="fa fa-edit"></i>@lang('site.edit')</button>
                     </div>

                  </form> <!-- end of form -->
               </div> 
               <!-- end of card body -->
            </div> 
             <!-- end of card -->
       </section>
   </div>
@endsection