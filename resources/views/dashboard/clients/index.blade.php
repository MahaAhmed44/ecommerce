@extends('layouts.dashboard.app')

@section('content')
     
     <div class="content-wrapper">
      <section class="content-header">
   	   <h1>@lang('site.clients')</h1>
   	   <ol class="breadcrumb" style="float: right;">
          <li><a href="{{ route('dashboard.index')}} "><i class="nav-icon fas fa-tachometer-alt"></i>@lang('site.dashboard') </a></li>&nbsp
          <li class="active">@lang('site.clients')</li>
        </ol>
   	   </section>
       <br>
   	   <section class="content">
   	   	     <div class="card card-primary">
               <div class="card-header with-border bg-gradient-danger" >
                  <h3 class="card-title text-white">@lang('site.clients')</h3><small class="text-white" style="font-size: 15px;">&nbsp&nbsp{{ $clients->total() }}</small><br><br>
                  <form action="{{ route('dashboard.clients.index') }}" method="get">
                     <div class="row">
                       <div class="col-md-4">
                         <input type="text" name="search" class="form-control" placeholder="@lang('site.search')" value="{{ request()->search }}">
                       </div>
                       <div class="col-md-4">
                         <button type="submit" class="btn bg-gradient-info"><i class="fa fa-search"></i>@lang('site.search')</button>
                         
                         <a href="{{ route('dashboard.clients.create') }}" class="btn bg-gradient-info"><i class="fa fa-plus"></i>@lang('site.add')</a>
                       </div>
                     </div>
                  </form> <!-- end of form -->
               </div> <!-- end of box header -->
               <div class="card-body">
                @if($clients->count() > 0)
                   <table class="table table-hover">
                     
                     <thead>
                      <tr>
                        <th>#</th>
                        <th>@lang('site.name')</th>
                        <th>@lang('site.phone')</th>
                        <th>@lang('site.address')</th>
                        <th>@lang('site.add_order')</th>
                        <th>@lang('site.action')</th>
                      </tr> 
                       <tbody>
                         @foreach($clients as $index=>$client)
                         <tr>
                           <td>{{$index + 1}}</td>
                          <td>{{$client->name}}</td>
                          <td>{{ is_array($client->phone) ? implode($client->phone,'-') : $client->phone}}</td>
                          <td>{{$client->address}}</td>
                          <td>
                           
                                <a href="{{ route('dashboard.orders.create',$client->id)}}" class="btn btn-info btn-sm">@lang('site.add_order')</a>
                                
                          </td>

                        
                          <td>
                            <a href="{{route('dashboard.clients.edit',$client->id)}}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i>@lang('site.edit')</a>
                             
                         
                            <form action="{{ route('dashboard.clients.destroy', $client->id)}}" method="post" style="display: inline-block;">
                              {{ csrf_field() }}
                              {{ method_field('delete') }}
                              <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i>@lang('site.delete')</button>
                            </form> <!-- end of form -->
                           
                          </td>
                         </tr>
                         @endforeach
                       </tbody>
                     </thead></table>
                     {{ $clients->appends(request()->query())->links() }}
                @else

                 <h2>@lang('site.no_date_found')</h2>

                @endif
                 
               </div>
            </div>  <!-- end of box -->
   	   </section>
   </div>
@endsection