@extends('layouts.dashboard.app')

@section('content')
     
     <div class="content-wrapper">
      <section class="content-header">
   	   <h1>@lang('site.orders')
        
       </h1>
   	   <ol class="breadcrumb" style="float: right;">
          <li><a href="{{ route('dashboard.index')}} "><i class="nav-icon fas fa-tachometer-alt"></i>@lang('site.dashboard') </a></li>&nbsp
          <li class="active">@lang('site.orders')</li>
        </ol>
   	   </section>
       <br>
   	   <section class="content">
   	   	   <div class="row">
              <div class="col-md-12">
                 <div class="card card-primary">
                    <div class="card-header bg-gradient-danger">
                    <h3 class="card-title" style="margin-bottom: 10px;">@lang('site.orders')</h3><small class="text-white" style="font-size: 15px;">&nbsp&nbsp{{ $orders->total() }}</small><br>
                      <form action="{{ route('dashboard.carts.index')}}" method="get">
                        <div class="row">
                           <div class="col-md-8">
                             <input type="text" name="search" class="form-control" placeholder="search">
                           </div>

                            <div class="col-md-4">
                             <button type="submit" class="btn btn-info"><i class="fa fa-search"></i>search</button>
                           </div>
                        </div> <!-- end of inside row -->
                      </form> 
                    </div> <!-- end of crad header -->
                      
                      @if ($orders->count() >=  0)
                        <div class="box-body table-responsive">
                           <table class="table table-hover">
                             <tr>
                               <!--th>@lang('site.client_name')</th-->
                               <th>@lang('site.price')</th>
                               <!--th>@lang('site.status')</th-->
                               <th>@lang('site.created_at')</th>
                               <th>@lang('site.action')</th>
                             </tr>

                             @foreach ($orders as $order)
                             <tr>
                              <!--td>{{$order->name}}</td-->
                              <td>{{ number_format($order->total_price,2)}}</td>
                             
                              <td>{{ $order->created_at->toFormattedDateString() }}</td>
                             
                              <td>
                                <a href="#" class="btn btn-info btn-sm"><i class="fa fa-edit"></i>@lang('site.edit')</a>

                              <form action="" method="post" style="display: inline-block;">
                              {{ csrf_field() }}
                              {{ method_field('delete') }}
                              <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i>@lang('site.delete')</button>
                            </form>
                              </td>
                             </tr>
                             @endforeach
                           </table>
                        </div>
                      @endif

                 </div> <!-- end of card -->
              </div> <!-- end of first column -->
             
             
           </div>  <!-- end of row -->
           
   	   </section>
   </div>
@endsection