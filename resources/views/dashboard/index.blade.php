@extends('layouts.dashboard.app')

@section('content')

   <div class="content-wrapper">
      <section class="content-header">
   	   <h1>@lang('site.dashboard')</h1>
   	   <ol class="breadcrumb" style="float: right;">
          <li class="active"> <i class="nav-icon fas fa-tachometer-alt"></i>@lang('site.dashboard')</li>
        </ol>
   	   </section><br>

   	   <section class="content">
   	   	   <div class="row">
                 <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{$categories_count}}</h3>

                <p>@lang('site.categories')</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="{{route('dashboard.categories.index')}}" class="small-box-footer">@lang('site.show')<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{$products_count}}</h3>

                <p>@lang('site.products')</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="{{route('dashboard.products.index')}}" class="small-box-footer">@lang('site.show')<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{$clients_count}}</h3>

                <p>@lang('site.clients')</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="{{route('dashboard.clients.index')}}" class="small-box-footer">@lang('site.show')<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>{{$users_count}}</h3>

                <p>@lang('site.users')</p>
              </div>
              <div class="icon">
               <i class="fas fa-users"></i>
              </div>
              <a href="{{route('dashboard.users.index')}}" class="small-box-footer">@lang('site.show')<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
           </div>
   	   </section>
   </div>

@endsection