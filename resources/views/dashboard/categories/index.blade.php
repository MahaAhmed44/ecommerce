@extends('layouts.dashboard.app')

@section('content')
     
     <div class="content-wrapper">
      <section class="content-header">
   	   <h1>@lang('site.categories')</h1>
   	   <ol class="breadcrumb" style="float: right;">
          <li><a href="{{ route('dashboard.index')}} "><i class="nav-icon fas fa-tachometer-alt"></i>@lang('site.dashboard') </a></li>&nbsp
          <li class="active">@lang('site.categories')</li>
        </ol>
   	   </section>
       <br>
   	   <section class="content">
   	   	     <div class="card card-primary">
               <div class="card-header with-border bg-gradient-danger"  >
                  <h3 class="card-title text-white">@lang('site.categories')</h3><small class="text-white" style="font-size: 15px;">&nbsp&nbsp{{ $categories->total() }}</small><br><br>
                  <form action="{{ route('dashboard.categories.index') }}" method="get">
                     <div class="row">
                       <div class="col-md-4">
                         <input type="text" name="search" class="form-control" placeholder="@lang('site.search')" value="{{ request()->search }}">
                       </div>
                       <div class="col-md-4">
                         <button type="submit" class="btn bg-gradient-info"><i class="fa fa-search"></i>@lang('site.search')</button>
                         
                         <a href="{{ route('dashboard.categories.create') }}" class="btn bg-gradient-info"><i class="fa fa-plus"></i>@lang('site.add')</a>
                        
                       </div>
                     </div>
                  </form> <!-- end of form -->
               </div> <!-- end of box header -->
               <div class="card-body">
                @if($categories->count() > 0)
                   <table class="table table-hover">
                     
                     <thead>
                      <tr>
                        <th>#</th>
                        <th>@lang('site.name')</th>
                        <th>@lang('site.products_count')</th>
                        <th>@lang('site.related_products')</th>
                        <th>@lang('site.action')</th>
                      </tr> 
                      </thead>
                       <tbody>
                         @foreach($categories as $index=>$category)
                         <tr>
                           <td>{{$index + 1}}</td>
                          <td>{{$category->name}}</td>
                          <td>{{$category->products->count()}}</td>
                           <td><a href="{{ route('dashboard.products.index',['category_id' => $category->id]) }}" class="btn btn-info btn-sm">@lang('site.related_products')</a></td>
                          <td>
                            
                              <a href="{{route('dashboard.categories.edit',$category->id)}}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i>@lang('site.edit')</a>
                             
                           
                            <form action="{{ route('dashboard.categories.destroy', $category->id)}}" method="post" style="display: inline-block;">
                              {{ csrf_field() }}
                              {{ method_field('delete') }}
                              <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i>@lang('site.delete')</button>
                            </form> <!-- end of form -->

                            <!--form action="{{ route('dashboard.categories.destroy', $category->id)}}" method="post" style="display: inline-block;">
                              {{ csrf_field() }}
                              {{ method_field('delete') }}
                              <button type="submit" class="btn btn-danger btn-sm">@lang('site.delete')</button>
                            </form--> <!-- end of form -->
                          </td>
                         </tr>
                         @endforeach
                       </tbody>
                     </thead></table>
                     {{ $categories->appends(request()->query())->links() }}
                @else

                 <h2>@lang('site.no_date_found')</h2>

                @endif
                 
               </div>
            </div>  <!-- end of box -->
   	   </section>
   </div>
@endsection