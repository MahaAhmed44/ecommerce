@extends('layouts.dashboard.app')

@section('content')
     
     <div class="content-wrapper">
      <section class="content-header">
   	   <h1>@lang('site.users')</h1>
   	   <ol class="breadcrumb" style="float: right;">
          <li><a href="{{ route('dashboard.index')}} "><i class="nav-icon fas fa-tachometer-alt"></i>@lang('site.dashboard') </a></li>&nbsp
          <li class="active">@lang('site.users')</li>
        </ol>
   	   </section>
       <br>
   	   <section class="content">
   	   	     <div class="card card-primary">
               <div class="card-header with-border bg-gradient-danger" >
                  <h3 class="card-title text-white">@lang('site.users')</h3><small class="text-white" style="font-size: 15px;">&nbsp&nbsp{{ $users->total() }}</small><br><br>
                  <form action="{{ route('dashboard.users.index') }}" method="get">
                     <div class="row">
                       <div class="col-md-4">
                         <input type="text" name="search" class="form-control" placeholder="@lang('site.search')" value="{{ request()->search }}">
                       </div>
                       <div class="col-md-4">
                         <button type="submit" class="btn btn-info"><i class="fa fa-search"></i>@lang('site.search')</button>
                        
                         <a href="{{ route('dashboard.users.create') }}" class="btn btn-info"><i class="fa fa-plus"></i>@lang('site.add')</a>
                       </div>
                     </div>
                  </form> <!-- end of form -->
               </div> <!-- end of box header -->
               <div class="card-body">
                @if($users->count() > 0)
                   <table class="table table-hover">
                     
                     <thead>
                      <tr>
                        <th>#</th>
                        <th>@lang('site.first_name')</th>
                        <th>@lang('site.last_name')</th>
                        <th>@lang('site.email')</th>
                        <th>@lang('site.image')</th>
                         <th>@lang('site.action')</th>
                      </tr> 
                       <tbody>
                         @foreach($users as $index=>$user)
                         <tr>
                           <td>{{$index + 1}}</td>
                          <td>{{$user->first_name}}</td>
                          <td>{{$user->last_name}}</td>
                          <td>{{$user->email}}</td>
                          <td><img src="{{ $user->image_path }}" style="width: 60px;height: 60px;" class="img-thumbnail" alt=""></td>
                          <td>
                            
                              <a href="{{route('dashboard.users.edit',$user->id)}}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i>@lang('site.edit')</a>
                            
                          
                            <form action="{{ route('dashboard.users.destroy', $user->id)}}" method="post" style="display: inline-block;">
                              {{ csrf_field() }}
                              {{ method_field('delete') }}
                              <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i>@lang('site.delete')</button>
                            </form> <!-- end of form -->
                            </td>
                         </tr>
                         @endforeach
                       </tbody>
                     </thead></table>
                     {{ $users->appends(request()->query())->links() }}
                @else

                 <h2>@lang('site.no_date_found')</h2>

                @endif
                 
               </div>
            </div>  <!-- end of box -->
   	   </section>
   </div>
@endsection