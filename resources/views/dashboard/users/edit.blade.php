@extends('layouts.dashboard.app')

@section('content')
     
     <div class="content-wrapper">
      <section class="content-header">
   	   <h1>@lang('site.users')</h1>
   	   <ol class="breadcrumb" style="float: right;">
          <li><a href="{{ route('dashboard.index') }}"><i class="nav-icon fas fa-tachometer-alt"></i>@lang('site.dashboard')</a></li>&nbsp
          <li><a href="{{ route('dashboard.users.index') }}">@lang('site.users')</a></li>&nbsp
          <li class="active">@lang('site.edit')</li>
        </ol>
   	   </section>
         <br>
   	   <section class="content">
   	   	    <div class="card card-primary">
               <div class="card-header bg-gradient-danger">
                  <h3 class="card-title">@lang('site.edit')</h3>
               </div> <!-- end of box header -->
               <div class="card-body">
               @include('partials._errors')
                  <form action="{{ route('dashboard.users.update',$user->id)}}" method="post" enctype="multipart/form-data">
                     {{ csrf_field() }}
                     {{ method_field('put') }}
                     <div class="form-group">
                       <label>@lang('site.first_name')</label>
                       <input type="text" name="first_name" class="form-control" value="{{ $user->first_name }}">
                     </div>

                     <div class="form-group">
                       <label>@lang('site.last_name')</label>
                       <input type="text" name="last_name" class="form-control" value="{{ $user->last_name}}">
                     </div>

                     <div class="form-group">
                       <label>@lang('site.email')</label>
                       <input type="email" name="email" class="form-control" value="{{ $user->email }}">
                     </div>

                      <div class="form-group">
                       <label>@lang('site.image')</label>
                       <input type="file" name="image" class="form-control image" >
                     </div>

                     <div class="form-group">
                       <img src="{{ $user->image_path }}" style="width:100px;height: 100px;" class="img-thumbnail image-preview" alt="">
                     </div>
                     
                     <div class="form-group">
                      <label>@lang('site.permissions')</label>
                   <!-- tabs -->
           <div class="row">
          <div class="col-12">
            
            <div class="card">
              <div class="card-header d-flex p-0">
              @php
               $models = ['users','categories','products','clients','orders'];
               $maps = ['create','read','update','delete'];
              @endphp

                <ul class="nav nav-pills ml-auto p-2">
                @foreach ($models as $index=>$model)
                  <li class="{{ $index == 0 ? 'active' : ''}}"><a class="nav-link active bg-gradient-danger" href="#{{ $model }}" data-toggle="tab">@lang('site.'.$model)</a></li>&nbsp&nbsp
                @endforeach
                  
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content">
                @foreach ($models as $index=>$model)
                  <div class="tab-pane {{ $index == 0 ? 'active' : ''}}"   id="{{ $model }}">
                   @foreach( $maps as $map)
                     <label><input type="checkbox"  name="permissions[]" {{ $user->hasPermission($map .'_'. $model) ? 'checked' : ''}} value="{{ $map .'_'. $model }}">@lang('site.'.$map)</label>
                   @endforeach
                  </div>
                @endforeach
               </div><!-- end of tab content -->
                <!-- end of tab navs -->
              </div>
            </div>
          </div>

        </div>
        </div>

          <!-- end of tabs -->
                       <div class="form-group">
                       <button type="submit" class="btn bg-gradient-danger"><i class="fa fa-edit"></i>@lang('site.edit')</button>
                     </div>

                  </form> <!-- end of form -->
               </div> 
               <!-- end of card body -->
            </div> 
             <!-- end of card -->
   	   </section>
   </div>
@endsection