<aside class="main-sidebar  elevation-4 " style="background:  linear-gradient(45deg, #1de099, #1dc8cd)">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link" style="background: linear-gradient(45deg, #1de099, #1dc8cd)">
      <img src="{{asset('panel/img/images (1).png')}}" style="height: 55px; width: 45px;" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light text-white">Shopping</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <!--img src="{{asset('panel/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image"-->
          <img src="{{ auth()->user()->image_path }}" style="width: 60px;height: 60px;" class="img-circle elevation-2">
        </div>
        <div class="info">
           <a href="#" class="d-block" style="color:white;">{{ auth()->user()->first_name}} {{ auth()->user()->last_name}}</a>
          <a href="#" class="d-block" style="color:white;"><i class="fa fa-circle text-success"></i>&nbsp&nbsp Online</a>
         
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
             <a href="{{ route('dashboard.index') }}" class="nav-link" style="color:white;">
                 <i class="fas fa-grip-horizontal"></i>
                  <p>@lang('site.dashboard')</p>
                </a>
           </li>
           

          
              <li class="nav-item">
             <a href="{{ route('dashboard.categories.index') }}" class="nav-link" style="color:white;">
                 <i class="fa fa-tasks"></i>
                  <p> @lang('site.categories')</p>
                </a>
           </li>
         

          
              <li class="nav-item">
             <a href="{{ route('dashboard.products.index') }}" class="nav-link" style="color:white;">
                 <i class="fa fa-window-restore"></i>
                  <p> @lang('site.products')</p>
                </a>
           </li>
          

           
              <li class="nav-item">
             <a href="{{ route('dashboard.clients.index') }}" class="nav-link" style="color:white;">
                 <i class="fa fa-user-plus"></i>
                  <p> @lang('site.clients')</p>
                </a>
           </li>
          

         
              <li class="nav-item">
             <a href="{{ route('dashboard.carts.index') }}" class="nav-link" style="color:white;">
                 <i class="fa fa-cart-plus"></i>
                  <p> @lang('site.orders')</p>
                </a>
           </li>
           

           

          
              <li class="nav-item">
             <a href="{{ route('dashboard.users.index') }}" class="nav-link" style="color:white;">
                 <i class="fa fa-users"></i>
                  <p> @lang('site.users')</p>
                </a>
           </li>
           
            
      </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>