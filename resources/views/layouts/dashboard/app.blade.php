<!DOCTYPE html>
<html dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
<head>
	<title>Shopping</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- bootstrap -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('panel/plugins/fontawesome-free/css/all.min.css') }}">
 
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
 
  <link rel="stylesheet" href="{{ asset('panel/plugins/tempusdominus-bootstrap-4/css
  /tempusdominus-bootstrap-4.min.css') }}">  
 
  <link rel="stylesheet" href="{{  asset('panel/plugins/icheck-bootstrap/icheck-bootstrap.min.css')  }}">
  
  <link rel="stylesheet" href="{{ asset('panel/plugins/jqvmap/jqvmap.min.css') }}">
  
  <link rel="stylesheet" href="{{ asset('panel/css/adminlte.min.css') }}">

  <link rel="stylesheet" href="{{asset('panel/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  
  <link rel="stylesheet" href="{{ asset('panel/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
 
  <link rel="stylesheet" href="{{ asset('panel/plugins/daterangepicker/daterangepicker.css') }}">
  
  <link rel="stylesheet" href="{{ asset('panel/plugins/summernote/summernote-bs4.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  

  <!-- new links -->
  

  @if (app()->getLocale() == 'ar')
   <link rel="stylesheet" href="{{ asset('panel/iff/AdminLTE-rtl.min.css') }}">
   <link rel="stylesheet" href="{{ asset('panel/iff/bootstrap-rtl.min.css') }}">
   <link rel="stylesheet" href="{{ asset('panel/iff/font-awesome-rtl.css') }}">
   <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css" integrity="sha384-vus3nQHTD+5mpDiZ4rkEPlnkcyTP+49BhJ4wJeJunw06ZAp+wzzeBPUXr42fi8If" crossorigin="anonymous">
   
   <style>
      body, h1, h2, h3, h4, h5, h6
      {
        font-family: 'Cairo', sans-serif !important;
      }
   </style>
  
   @endif
  

</head>
<body>
 <div class="wrapper">
  <header class="main-header">
    <nav  class="navbar navbar-expand  navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link " data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars bg-danger"></i></a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search text-danger"></i>
          </button>
        </div>
      </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
    
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link text-danger" data-toggle="dropdown" href="#">
          @lang('site.language')
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
       <ul>
       @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
        <li>
            <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}" class="dropdown-item">
                {{ $properties['native'] }}
            </a>
        </li>
        @endforeach
      </ul>
          
          <div class="dropdown-divider"></div>
         </div>
      </li>
<!-- new item added ************************************************************ -->
       
      <!-- logout -->
      
      <li class="nav-item">
      <a class="nav-link text-danger" data-widget="control-sidebar" data-slide="true" role="button"
       href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">@lang('site.logout')</a>
       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
        <!--a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i>
        </a-->
      </li>
    </ul>
  </nav>
           </header>
     @include('layouts.dashboard._aside')

      @yield('content')

      @include('partials._session')
<!-- footer -->
   <footer class="main-footer">
   <div class="row">
     <div class="col-md-5">
       
     </div>
     <div class="col-md-3" style="float: right;">
       <a href="#" class="text-info" ><i class="fab fa-facebook" style="font-size: 30px;"></i></a>&nbsp
       <a href="#" class="text-info"><i class="fab fa-twitter-square" style="font-size: 30px;"></i></a>&nbsp
       <a href="#" class="text-info"><i class="fab fa-instagram-square" style="font-size: 30px;"></i></a>
     </div>
     <div class="col-md-4">
     
     </div>
    </div>
  </footer>

 </div>


 <script src="{{ asset('panel/plugins/jquery/jquery.min.js') }}"></script>

<script src="{{ asset('panel/plugins/jquery-ui/jquery-ui.min.js') }} "></script>

<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>

<script src="{{ asset('panel/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<script src="{{ asset('panel/plugins/chart.js/Chart.min.js') }}"></script>

<script src="{{ asset('panel/plugins/sparklines/sparkline.js') }}"></script>

<!--script src="{{ asset('panel/plugins/jqvmap/jquery.vmap.min.js') }}"></script-->
<script src="{{ asset('panel/plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>

<script src="{{ asset('panel/plugins/jquery-knob/jquery.knob.min.js') }}"></script>

<script src="{{ asset('panel/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('panel/plugins/daterangepicker/daterangepicker.js') }}"></script>

<script src="{{ asset('panel/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>

<script src="{{ asset('panel/plugins/summernote/summernote-bs4.min.js') }}"></script>

<script src="{{ asset('panel/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>

<script src="{{asset('panel/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('panel/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>

<script src="{{ asset('panel/js/adminlte.js') }}"></script>

<script src="{{ asset('panel/js/pages/dashboard.js') }}"></script>

<script src="{{ asset('panel/js/demo.js') }}"></script>
<script src="{{ asset('panel/plugins/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('panel/js/jquery.number.min.js') }}"></script>

<script src="{{ asset('panel/js/custom/order.js') }}"></script>


<script>
    $('.image').change(function () {
      if(this.files && this.files[0])
      {
        var reader = new FileReader();
        reader.onload = function (e)
        {
          $('.image-preview').attr('src',e.target.result);
        }
        reader.radAsDataURL(this.files[0]);
      }
      
    });
</script>
</body>
</html>

 </body>
</html> 