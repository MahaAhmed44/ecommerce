<?php

return [

      'dashboard' => 'الرئيسية',

      'add'       => 'اضف',
      'create'    =>  'اضافة',
      'read'    =>  'عرض',
      'edit'       => 'تعديل',
      'update'       => 'تعديل',
      'delete'       => 'حذف',
      'search'       => 'بحث',

      'password'     =>  'كلمة المرور',
      'password_confirmation'     =>  'تأكيد كلمة المرور',
      'added_successfully'       =>  'تم اضافة البيانات بنجاح',
      'updated_successfully'       =>  'تم تعديل البيانات بنجاح',
      'deleted_successfully'       =>  'تم حذف البيانات بنجاح',

      'no_date_found' => 'للاسف لا يوجد اي سجلات',
      'no_records' => 'للاسف لا يوجد اي سجلات',

      'users'     => 'المشرفين',
      'first_name' =>  'الاسم الاول',
      'last_name' =>  'الاسم الاخير',
      'email'     =>  'البريد الالكتروني',
      'image'     => 'صورة',
      'action'    =>  'اكشن',
      'permissions' => 'الصلاحيات',
      'categories'  => 'الاقسام',
      'all_categories'  => 'كل الاقسام',
      'category'       =>  'القسم',
      'products'    => 'المنتجات',
      'name'        => 'الاسم',
      'description'  => 'الوصف',

      'purchase_price'   => 'سعر الشراء',
      'sale_price'   => 'سعر البيع',
      'stock'   => 'المخزن',
      'profit_percent'  => 'المكسب',
      'products_count'  => 'عدد المنتجات',
      'related_products' => 'المنتجات المرتبطة',

      'clients'         =>  'العملاء',

      'address'         =>  'العنوان',
      'phone'         =>  'التلفون',

      'orders'         => 'الطلبات',
      'add_order'         => 'اضف طلب',

      'product'          =>  'المنتج',
      'quantity'         =>   'الكمية',
      'price'            => 'السعر',
      'total'            => 'المجموع',
      'carts'            => 'الطلبات',
      'client_name'      =>  'اسم العميل',
      'show_products'    => 'عرض المنتجات',
      'created_at'       => 'تم اضافته',
      'show'             => 'عرض',
      'language'         =>  'اللغة',
      'logout'         => 'تسجيل الخروج',

];