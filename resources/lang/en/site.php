<?php

return [

      'dashboard' => 'Dashboard',

      'add'       => 'add',
      'create'    =>  'create',
      'read'    =>  'read',
      'edit'       => 'edit',
      'update'       => 'update',
      'delete'       => 'delete',
      'search'       => 'search',

      'password'     =>  'password',
      'password_confirmation'     =>  'password confirmation',
      'added_successfully'       =>  'added successfully',
      'updated_successfully'       =>  'updated successfully',
      'deleted_successfully'       =>  'deleted successfully',

      'no_date_found' => 'no date found',
      'no_records' => 'no records',

      'users'     => 'users',
      'first_name' =>  'first name',
      'last_name' =>  'last name',
      'email'     =>  'email',
      'image'     => 'image',
      'action'    =>  'action',
      'permissions' => 'permissions',
      'categories'  => 'categories',
      'all_categories'  => 'all categories',
      'category'       =>  'category',
      'products'    => 'products',
      'name'        => 'name',
      'description'  => 'description',

      'purchase_price'   => 'purchase price',
      'sale_price'   => 'sale price',
      'stock'   => 'stock',
      'profit_percent'  => 'profit percent',
      'products_count'  => 'products percent',
      'related_products' => 'related products',

      'clients'         =>  'clients',

      'address'         =>  'address',
      'phone'         =>  'phone',

      'orders'         => 'orders',
      'add_order'         => 'add order',

      'product'          =>  'product',
      'quantity'         =>   'quantity',
      'price'            => 'price',
      'total'            => 'total',
      'carts'            => 'carts',
      'client_name'      =>  'client name',
      'show_products'    => 'show products',
      'created_at'       => 'created at',
      'show'             => 'show',
      'language'         => 'language',
      'logout'         => 'logout',

];