<?php

Route::group(
[
      'prefix' => LaravelLocalization::setLocale(),'middleware' => [ 'localeSessionRedirect', 
      'localizationRedirect', 'localeViewPath' ]],
 function(){
 	  Route::prefix('dashboard')->name('dashboard.')->middleware(['auth'])->group(function() {
     
     Route::get('/index','DashboardController@index')->name('index');

      // category routes
     Route::resource('categories','CategoryController')->except(['show']);

      // product routes
     Route::resource('products','ProductController')->except(['show']);

     // client routes
     Route::resource('clients','ClientController')->except(['show']);
     Route::resource('orders','OrderController')->except(['show']);

     // order routes
    Route::resource('carts','CartController');
    //Route::get('/carts/{order}/products','CartController@products')->name('orders.products');

      // user routes
     Route::resource('users','UserController')->except(['show']);

   }); //end of dashboard routes

 });

